from aacore.sniffers import sniffer
import RDF


@sniffer("rss-tag-soup")
class FeedSniffer(object):
    def __init__(self, request=None, model=None):
        self.request = request
        self.model = model

    def test(self):
        q = '''
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX hdr: <http://www.w3.org/2011/http-headers#>
        ASK {
            { ?subject dct:format ?object .}
            UNION
            { ?subject hdr:content-type ?object .}
            FILTER (?object = "application/rss+xml"  ||
                    ?object = "application/atom+xml" ||
                    ?object = "application/xml").
        }'''
        results = RDF.Query(q, query_language="sparql").execute(self.model)
        return results.get_boolean()

    def sniff(self):
        print("sniffed an rss or atom feed")
        return self.request.text
