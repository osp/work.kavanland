from django import forms
from textwrap import dedent


class ResourceForm(forms.Form):
    node = forms.CharField()


class SparqlForm(forms.Form):
    qs = dedent("""
    PREFIX aa:<http://activearchives.org/terms/>

    SELECT DISTINCT ?s
    WHERE { 
        ?s aa:nature "parole"@fr
    }
    """)
    query = forms.CharField(widget=forms.Textarea, initial=qs)
